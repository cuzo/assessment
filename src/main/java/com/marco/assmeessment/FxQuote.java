package com.marco.assmeessment;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class FxQuote {
	Map<String, Double> rates;
	
	public FxQuote(String filename){
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		rates = new HashMap<String, Double>();
		try{
			InputStream sourcePath = FxQuote.class.getClassLoader().getResourceAsStream(filename);
			br = new BufferedReader(new FileReader(filename));           
			while ((line = br.readLine()) != null) {
				if(!line.contains("C1,C2,Rate")){
					// use comma as separator
					String[] row = line.split(cvsSplitBy);
					boolean badForm = false;
					double rate = 0.0;
					try{
						rate = Double.parseDouble(row[2]);
					}catch(NumberFormatException e){
						badForm = true;
					}catch(NullPointerException  e){
						badForm = true;
					}
					if(!badForm){
						rates.put(row[0].toUpperCase()+":"+row[1].toUpperCase(),  rate);
					}else{
						System.out.println("Input is not well formatted, skipping");
					}
				}
			}
		}catch (FileNotFoundException e) {
			            e.printStackTrace();
		} catch (IOException e) {
			            e.printStackTrace();
		} finally {
			if (br != null) {
				try{
					   br.close(); 
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		} 
	}
	
	public double getFxRate(String sourceCcy, String DestCcy){
		if(sourceCcy == null || DestCcy == null || sourceCcy.equals("") || DestCcy.equals("")){
			System.out.println("Input currency cannot be empty");
			return -1.0;
		}
		sourceCcy = sourceCcy.toUpperCase();
		DestCcy = DestCcy.toUpperCase();
		String key = sourceCcy+":"+DestCcy;
		double rate  = getValue(key);
		double secondRate = -1;
		if(rate == -1){
			rate = getValue(sourceCcy+":USD");
			secondRate = getValue("USD:"+DestCcy);
			if(rate == -1 || secondRate == -1){
				System.out.println("Cannot convert between two quantities. No FX rate exists");
				return -1.0;
			}else{
				rate = rate/secondRate;
			}
		}
		return rate;
	}
	
	private double getValue(String key){
		double rate;
		try{
			rate = rates.get(key);			
		}catch(NullPointerException e){
			return -1;
		}
		return rate;
	}

}
