package com.marco.assmessment;
import static org.junit.Assert.*;


import java.io.FileNotFoundException;

import org.junit.Test;

import com.marco.assmeessment.FxQuote;

public class TestFxQuote {
	
	String badDouble = "badDouble.csv";
	String smallBatch = "smallBatch.csv";
	

	@Test
	public void tesFileNotFound() {
		try{
			FxQuote fq = new FxQuote("Not\\a\\real\\path");
		}catch (Exception e) {
			assertEquals(FileNotFoundException.class, e.getClass());
		}
	}

	
	@Test
	public void testEmptyCcyName() {
		FxQuote fq = new FxQuote(badDouble);
		assertEquals(-1.0, fq.getFxRate("", null), 0.001);
	}
	
	@Test
	public void testCorrectInput() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(0.14, fq.getFxRate("MXN","USD"), 0.01);
	}
	
	@Test
	public void testLowerCaseInput() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(0.14, fq.getFxRate("mxn","usd"), 0.01);
	}
	
	@Test
	public void testNonDirectQuotes() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(2.5, fq.getFxRate("AED","AUD"), 0.01);
	}
	
	@Test
	public void testDirectQuotes() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(-1.0, fq.getFxRate("AED","MXN"), 0.01);
	}
	
	@Test
	public void testNonExistantCcy() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(-1.0, fq.getFxRate("AED","PPP"), 0.01);
	}
	
	@Test
	public void testMostRecentRate() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(3.0, fq.getFxRate("GTK","JPN"), 0.01);
	}

	@Test
	public void testSkipsBadInput() {
		FxQuote fq = new FxQuote(smallBatch);
		assertEquals(3.0, fq.getFxRate("GTK","JPN"), 0.01);
	}
}
